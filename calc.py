def suma(sumando1, sumando2):
    print("suma", sumando1, "+", sumando2, "=", sumando1+sumando2)
    return sumando1+sumando2


def resta(restando1, restando2):
    print("resta", restando1, "-", restando2, "=", restando1-restando2)
    return restando1-restando2


suma(1, 2)
suma(3, 4)
resta(6, 5)
resta(8, 7)
'''el return en este caso no haría falta, ya que estoy
imprimiendo directamente dentro de la función, pero en 
un programa mas largo lo podría necesitar'''